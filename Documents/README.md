(--------------------------Readme---------------------------------)
Jeu de TRAVERSE par Maxime Lescaud, Paul Brugat et Fabien Laurencet.

Installation :
-Logiciel requis : 
	Free Pascal Compiler (FPC)
	Installation de FPC : Dans un terminal entrez la commande suivante : sudo apt-get install fpc
-Installation du jeu : Se diriger dans le répertoire contenant le programme Traverse.pas ainsi que les unités suivantes(UType.pas,UInitialisation.pas,UJeu.pas,UInterface.pas,U2joueurs.pas,U4joueurs.pas,UIA.pas,glib2d.pas), ensuite utiliser la commande : fpc Traverse.pas , une fois la compilation terminée le jeu sera installé sous le nom de Traverse .

Éxecution :
Pour lancer le jeu, dans un terminal, en se situant toujours dans le répertoire contenant le jeu, utiliser la commande suivante : ./Traverse

Menu :
Pour lancer une partie, cliquer sur "PLAY" sur la page d'accueil, vous accéderez ensuite à une nouvelle page vous laissant le choix entre différents modes de jeux : humains vs humains (2 joueurs ou 4 joueurs) ou Humain vs IA. Après avoir fait votre choix la partie se lancera selon le mode jeu choisi.
Pour changer le thème cliquer sur "THEMES" et sélectionner parmis les trois thèmes proposés, pour retourner au menu cliquez sur "RETOUR".
Pour afficher le crédit de jeu cliquer sur "CREDIT", pour quitter celui-ci cliquez sur "RETOUR".

Règles et guide d'utilisation du jeu:

Règles :

Traverse se joue à deux joueurs ou 4 joueurs sur un plateau de 10x10 cases. On distingue les cases en bordure et les cases intérieures. Le   vainqueur de Traverse est celui qui aura déplacé en premier tous ses pions de sa zone de départ jusqu'à la zone d'arrivée se trouvant du côté opposé du plateau.
 
 -Début du jeu 
Chaque joueur possède huit pions de sa couleur: 2 carrés, 2 losanges, 2 triangles et 2 cercles. 

 -Les pions 
Le mouvement des pions se fait en fonction de leur forme: 
● les carrés se déplacent horizontalement et verticalement. 
● les losanges se déplacent en diagonale. 
● les triangles se déplacent en diagonale vers l'avant et tout droit vers l'arrière. 
● les cercles se déplacent dans les huit directions .
  
Les joueurs jouent à tour de rôle, en déplaçant un seul pion par tour. Deux pions ne peuvent occuper la même case. Passer son tour n'est pas autorisé à moins qu'un joueur soit piégé et ne puisse déplacer aucun pion. Dans ce cas, le joueur passe son tour jusqu'à ce qu'il puisse de   nouveau exécuter un coup légal. À chaque tour, le joueur choisit entre un déplacement simple et une série de sauts. 
  
 -Déplacement simple 
Un pion peut être déplacé vers une case adjacente en respectant les directions selon le type du pion. 
  
Sauts 
Un pion peut sauter par dessus un autre pion de forme et de couleur quelconque se trouvant sur une case adjacente. La direction d'un saut doit   être conforme aux directions que le pion peut exécuter en déplacement simple. Dans un même tour, un pion peut effectuer plusieurs sauts successifs tant qu'il existe des pions à enjamber. De cette manière, il est parfois possible de parcourir le plateau entier en un seul coup. Cependant, contrairement à d'autres jeux comme les dames classiques, le saut se fait sans prise. Tous les pions restent ainsi en jeu jusqu'à la fin de la partie. 


 -Angles et côtés du plateau 
Un pion peut librement entrer et quitter sa zone d'arrivée. Sinon, à l'exception de sa zone d'arrivée, le déplacement d'un pion ne doit jamais finir sur une case en bordure. 
Cependant, un pion peut visiter temporairement une case en bordure s'il la quitte de nouveau pendant le même tour. 
  
 -Fin du jeu 
Le match est terminé lorsque : 
● l'un des joueurs a réussi à réunir tous ses pions dans la zone d'arrivée(la position des pions est sans importance). Ce joueur est alors déclaré vainqueur; 
● l'un des joueurs n'a pas libéré sa zone de départ à temps : en effet, la zone de départ doit être évacuée au plus tard après le 30ème tour. Dès   le 31ème tour, tout pion qui stationne à la fin d'un coup dans sa propre zone de départ(les sauts intermédiaires sont autorisés) provoque aussitôt la perte de la partie; 
● la même position vient de se présenter pour la troisième fois: afin d'éviter les matchs interminables, une partie est déclarée nulle lors d'une triple répétition de la position, consécutivement ou non. 

 -Guide d'utilisation :
Pour jouer, cliquez une première fois sur un pion appartenant à l'équipe à qui le tour est, les mouvements et les sauts possibles s'afficheront( mouvements normaux: jaune, saut : violet), cliquez ensuite sur une case illuminée (jaune ou violet), pour déplacer le pion et terminer le tour. Si vous avez sélectionné un pion et que vous souhaitez changer de sélection pas de soucis, il vous suffit de cliquer sur le pion que vous voulez sélectionner. Sachez qu'une fois un tour terminé aucun retour en arrière n'est autorisé, alors attention aux erreurs et soyez vigilants sur vos mouvements. Dans le cas d'une succession de sauts, le pion sélectionné peut effectuer une succession de sauts infinie (si souhaitée et tant que ceux-ci sont légaux) dans le même tour jusqu'à ce que le joueur décide avoir terminé son tour, il peut même retourner à sa position initiale du début de tour. Afin de terminer une succession de saut, cliquer avec le clic droit n'importe où sur la page et ainsi le tour sera terminé.
Vous savez désormais comment jouer à TRAVERSE alors let's play!


