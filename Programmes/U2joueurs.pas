
Unit U2joueurs;
interface

USES crt,UType,UInitialisation,UJeu,UInterface,glib2d;

FUNCTION ExeMove (Jeu : Game; Pion: Pions ): Game;
FUNCTION Victoire2Joueurs (Jeu :Game ):Boolean;
implementation

(*--------------------------------------------------------------
 --nom            : Victoire2Joueurs
 --rôle           :  Permet de définir la victoire d'un joueur 
 --pré-conditions : Un tableau de jeu (Type : Game)
 --résultat       : Un booleén (True = victoire)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Victoire2Joueurs (Jeu :Game ):Boolean;
VAR
   i,WinTeamI,WinTeamII,LoseTeamI,LoseTeamII : integer;
BEGIN
   WinTeamI:=0;
   WinTeamII:=0;
   LoseTeamI:=0;
   LoseTeamII:=0;
   For i:= min to max do
   Begin
      Case Jeu.Tabjeu[i,min].team of
        1 : LoseTeamI:=LoseTeamI+1;
        2 : WinTeamII:=WinTeamII+1;
      End;{ case }
      Case Jeu.Tabjeu[i,max].team of
        1 : WinTeamI:=WinTeamI+1;
        2 : LoseTeamII:=LoseTeamII+1;
      End; { case }
   End;
   Victoire2Joueurs:=(((Jeu.nbcoups>60)and((LoseTeamI>0)or(LoseTeamII>0)))or(WinteamI=8)or(WinteamII=8));
END; { Victoire2Joueurs }

(*--------------------------------------------------------------
 --nom            : ExeMove
 --rôle           : Permet l'éxecution d'une succession de sauts
 --pré-conditions : Le Jeu et un pion(Types : Game,Pions)
 --résultat       : Un jeu (Type : Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION ExeMove (Jeu : Game; Pion: Pions ): Game;
VAR
   y   : position;
   k   : integer;
BEGIN
   k:=0;
   y:=AssignPos(12,12);
   While((VerifPossibleJump(Jeu,jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee])and((y.abscisse<>0)and(y.ordonnee<>0)))or(Pion.pos.abscisse=min)or(Pion.pos.abscisse=max)or((Pion.team=1)and(Pion.pos.ordonnee=1))or((Pion.team=2)and(Pion.pos.ordonnee=10)))do
   Begin
      PossibleJump(Jeu,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee]);
      y:=MouseControl(Jeu);
      If(y.abscisse<>0)and(y.ordonnee<>0)then
         if((Jeu.tabjeu[y.abscisse,y.ordonnee].genre=CaVi)and(VerifSautPion(Jeu,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee],Jeu.tabjeu[y.abscisse,y.ordonnee])))then
         Begin            
            Jeu:=EchangePions(Jeu,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee],Jeu.tabjeu[y.abscisse,y.ordonnee]);
            Jeu.nbcoups:=jeu.nbcoups-1;
            Pion:=Jeu.tabjeu[y.abscisse,y.ordonnee];
         End;
   End;
   Jeu.nbcoups:=Jeu.nbcoups-k;
   ExeMove:=Jeu;
END; { ExeMove }


BEGIN
END.
