
(*
 -------------------------------------------------------------------
 Nom Programme    :  testu1.pas
 Auteur           :  Laurencet Fabien <laurencetf@eisti.eu>
 Description      : Tests unitaires pour le unit UInitialisation
 Date de création :  Wed May 11 08:01:44 2016
 Compilation      :  fpc
 Execution        :  shell
 -------------------------------------------------------------------
 *)

PROGRAM  testu1;
USES crt, UInitialisation;


 
(* procedure vérifiant si les carrés sont bien placés au début du jeu pour les deux joueurs
 *
 *)

Procedure verifCar (plateau :GameTab );
Begin

   if (plateau[2,1].genre='Carré') and (plateau[9,1].genre='Carré') and (plateau[2,10].genre='Carré') and (plateau[9,10].genre='Carré') then
   begin
      textcolor(green);
      writeln ('Les carrés des joueurs 1 et 2 sont présents et bien placés');
      textcolor(0);
   end
   else
   begin
      textcolor(red);
      writeln('Les carrés ne sont pas bien initialisés');
      textcolor(0);
   end;   
end; { verifcar }




(*--------------------------------------------------------------
 --nom            : verifrond
 --rôle           :  vérifie si les ronds sont correctement positions pour les deux joueurs
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)

PROCEDURE verifRond (plateau :gameTab );
BEGIN
   
   if (plateau[5,1].genre='Rond') and (plateau[6,1].genre='Rond') and (plateau[5,10].genre='Rond') and (plateau[6,10].genre='Rond') then
   begin
      textcolor(green);
      writeln ('Les ronds des joueurs 1 et 2 sont présents et bien placés');
      textcolor(0);
   end
   else
   begin
      textcolor(red);
      writeln('Les ronds ne sont pas bien initialisés');
      textcolor(0);
   end;   
END;

(*--------------------------------------------------------------
 --nom            : verifLos
 --rôle           :   vérifie si les losanges sont correctement positions pour les deux joueurs
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE verifLos (Plateau :GameTab );
BEGIN
   if (plateau[4,1].genre='Losange') and (plateau[7,1].genre='Losange') and (plateau[4,10].genre='Losange') and (plateau[7,10].genre='Losange') then
   begin
      textcolor(green);
      writeln ('Les losanges des joueurs 1 et 2 sont présents et bien placés');
      textcolor(0);
   end
   else
   begin
      textcolor(red);
      writeln('Les losanges ne sont pas bien initialisés');
      textcolor(0);
   end;   
END;

(*--------------------------------------------------------------
 --nom            : verifTriangle
 --rôle           :  décrire le but de la procédure
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE verifTriangle (plateau :GameTab );
BEGIN
     if (plateau[3,1].genre='Triangle') and (plateau[8,1].genre='Triangle') and (plateau[3,10].genre='Triangle') and (plateau[8,10].genre='Triangle') then
   begin
      textcolor(green);
      writeln ('Les triangles des joueurs 1 et 2 sont présents et bien palcés');
      textcolor(0);
   end
   else
   begin
      textcolor(red);
      writeln('Les triangles ne sont pas bien initialisés');
      textcolor(0);
   end;   
END;

var
   plateau : GameTab;

BEGIN
   plateau:=InitGameTab().tabjeu;
   verifCar(plateau);
   verifRond(plateau);
   verifLos(plateau);
   verifTriangle(plateau);
END.
