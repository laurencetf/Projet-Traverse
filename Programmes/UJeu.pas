Unit Ujeu;

Interface
Uses CRT, UType,UInitialisation;
FUNCTION EchangePions (Jeu : Game; Pion, Caselibre : Pions ): Game;
FUNCTION PassTour (Jeu : Game ):Boolean;
FUNCTION SoustraitPos (pos,posbis :position ):position;
FUNCTION VerifSautPion (Jeu : Game; pion,caselibre : Pions ):boolean;
FUNCTION VerifCoup (Jeu : Game; pion, caselibre : Pions ):Boolean;
FUNCTION VerifPossibleJump (Jeu : Game; Pion :Pions ):boolean;
FUNCTION VerifNul (Jeu : Game ):Boolean;
FUNCTION VerifMoveBordure(Jeu : Game;  pion :Pions ):boolean;
implementation

(*--------------------------------------------------------------
 --nom            : EchangePions
 --rôle           : Permet d'échanger la position d'un pion et d'une case vide suite à un déplacement
 --pré-conditions : Un tableau de jeu, un pion et une case vide
 --résultat       :  Le jeu suite à un déplacement (Tableau de jeu et nombre de coups) ( de type jeu)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION EchangePions (Jeu : Game; Pion, Caselibre : Pions ): Game;
VAR
   Res : Game;
BEGIN
   Res.Tabjeu:=Jeu.tabjeu;
   Res.nbEquipes:=Jeu.nbequipes;
   Res.TabJeu[Caselibre.pos.abscisse,Caselibre.pos.ordonnee].genre:=Pion.genre;
   Res.TabJeu[Caselibre.pos.abscisse,Caselibre.pos.ordonnee].team:=Pion.team;
   Res.TabJeu[Pion.pos.abscisse,pion.pos.ordonnee].genre:=CaVi;
   Res.TabJeu[Pion.pos.abscisse,pion.pos.ordonnee].team:=0;
   Res.theme:=Jeu.theme;
   Res.nbcoups:=Jeu.nbcoups+1;
   EchangePions:=Res;
END; { EchangePions }


(*--------------------------------------------------------------
 --nom            : SoustraitPos
 --rôle           : Soustrait deux positions
 --pré-conditions : Deux positions (x,y) (Type :Position)
 --résultat       : Une Position (Type : Position)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION SoustraitPos (pos,posbis :position ):position;
VAR
   res : position;
BEGIN
   res.abscisse:=posbis.abscisse-pos.abscisse;
   res.ordonnee:=posbis.ordonnee-pos.ordonnee;
   SoustraitPos:=res;   
END; { SoustraitPos }


(*--------------------------------------------------------------
 --nom            : VerifCoupTriangle
 --rôle           : Vérifie si le déplacement d'un triangle est possible
 --pré-conditions :  Un tableau de jeu, un pion et une case libre (Type : Game; Pions ) et un choix du type de déplacement(normal ou saut) (Type :integer)
 --résultat       :  un booléen
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifCoupTriangle (Jeu : Game; pion, caselibre : Pions; choix: integer ):boolean;
Var
   res  : boolean;
   Posi : Position;
BEGIN
   Posi:=SoustraitPos(Pion.pos,caselibre.pos);
   if(choix=1)then
      Begin
         Case pion.team of
           1 : res:=(((Posi.abscisse=-1)and(Posi.ordonnee=1))or((Posi.abscisse=0)and(Posi.ordonnee=-1))or((Posi.abscisse=1)and(Posi.ordonnee=1)));
           2 : res:=(((Posi.abscisse=-1)and(Posi.ordonnee=-1))or((Posi.abscisse=1)and(Posi.ordonnee=-1))or((Posi.abscisse=0)and(Posi.ordonnee=1)));
           3 : res:=(((Posi.abscisse=1)and(Posi.ordonnee=-1))or((Posi.abscisse=1)and(Posi.ordonnee=1))or((Posi.abscisse=-1)and(Posi.ordonnee=0)));
           4 : res:=(((Posi.abscisse=-1)and(Posi.ordonnee=-1))or((Posi.abscisse=-1)and(Posi.ordonnee=1))or((Posi.abscisse=1)and(Posi.ordonnee=0)));
         end; { case }
      End
      else
         Case pion.team of
           1 : res:=(((Posi.abscisse=-2)and(Posi.ordonnee=2))or((Posi.abscisse=0)and(Posi.ordonnee=-2))or((Posi.abscisse=2)and(Posi.ordonnee=2)));
           2 : res:=(((Posi.abscisse=-2)and(Posi.ordonnee=-2))or((Posi.abscisse=2)and(Posi.ordonnee=-2))or((Posi.abscisse=0)and(Posi.ordonnee=2)));
           3 : res:=(((Posi.abscisse=2)and(Posi.ordonnee=-2))or((Posi.abscisse=2)and(Posi.ordonnee=2))or((Posi.abscisse=-2)and(Posi.ordonnee=0)));
           4 : res:=(((Posi.abscisse=-2)and(Posi.ordonnee=-2))or((Posi.abscisse=-2)and(Posi.ordonnee=2))or((Posi.abscisse=2)and(Posi.ordonnee=0)));
         end; { case }
   VerifCoupTriangle:=res;
END; { VerifCoupTriangle }



(*--------------------------------------------------------------
 --nom            : VerifCoup
 --rôle           : Vérifie si un coup est possible 
 --pré-conditions : Un tableau de jeu, un pion et une case libre (Type : Game; Pions )
 --résultat       : Un booléen 
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifCoup (Jeu : Game; pion, caselibre : Pions ):Boolean;
VAR
   res  : Boolean;
   Posi : position;
BEGIN
   Posi:=SoustraitPos(Pion.pos,caselibre.pos);
   If((((posi.abscisse<=1)and(posi.abscisse>=-1))and((posi.ordonnee<=1)and(posi.ordonnee>=-1)))and
      (caselibre.genre=CaVi)and
      ((caselibre.pos.abscisse<>max)and(caselibre.pos.abscisse<>min))and
      (((Pion.team=1)and(caselibre.pos.ordonnee<>min))or((Pion.team=2)and(caselibre.pos.ordonnee<>max))or((Pion.team=3)and(caselibre.pos.abscisse<>min))or((Pion.team=4)and(caselibre.pos.abscisse<>max))))then
   Begin
      Case pion.genre of
        Carre : res:=((posi.abscisse=0)or(posi.ordonnee=0));
        Rond  : res:=true;
        Losa  : res:=((posi.abscisse<>0)and(posi.ordonnee<>0));
        Tria  : res:=VerifCoupTriangle(Jeu,Pion,Caselibre,1);
      End; { case }
   End
   else
   Begin
      res:=false;
   End;
   VerifCoup:=res;
END; { VerifCoup }




(*--------------------------------------------------------------
 --nom            : CompareTab
 --rôle           : Compare deux tableaux de jeu
 --pré-conditions : Deux tableaux de jeu (Type : GameTab)
 --résultat       : un booléen true si les deux tableaux sont égaux
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION CompareTab (tab1,tab2 : Gametab):boolean;
VAR
   res      : boolean;
   i,k,compteur : integer;
BEGIN
   compteur:=0;
   for i:=min to max do
      Begin
         for k:=min to max do
            Begin
               if((tab1[i,k].genre=tab2[i,k].genre)and(tab1[i,k].team=tab2[i,k].team))then
                  compteur:=compteur+1;
            End;
      End;
   res:=(compteur=100);
   CompareTab:=res;
END; { CompareTab }



(*--------------------------------------------------------------
 --nom            : VerifNul
 --rôle           : Vérifie si un match est nul 
 --pré-conditions : Le jeu actuel (Type : Game)
 --résultat       : Un booléen (Type :Boolean)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifNul (Jeu : Game ):Boolean;
var
   res : boolean;
   i   : integer;
BEGIN
   i:=0;
   While(Jeu.ListeDeCoups^.suivant<>nil)do
   Begin
      If(CompareTab(Jeu.ListeDeCoups^.valeur,Jeu.TabJeu))then
      Begin
         i:=i+1;
      end;
      Jeu.ListeDeCoups:=Jeu.ListeDeCoups^.suivant;
   End;
   Res:=(i>=3);
   VerifNul:=Res;
   if(VerifNul)then
      Writeln('Match Nul');
END; { VerifNul }

(*--------------------------------------------------------------
 --nom            : PassTour
 --rôle           : Permet de passer le tour si le joueur ne peut effectuer aucun mouvement
 --pré-conditions : Un tableau de jeu (Type : Game)
 --résultat       : un booléen
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION PassTour (Jeu : Game ):Boolean;
VAR
   res : boolean;
   i,j :  integer;
BEGIN
   Res:=False;
   for i:=min to max do
      for j:=min to max do
         If(Jeu.tabjeu[i,j].team=((Jeu.nbcoups mod 2)+1))then
            Res:=Res or(VerifPossibleJump(Jeu, Jeu.tabjeu[i,j])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i-1,j-1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i,j-1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i+1,j-1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i+1,j])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i+1,j+1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i,j+1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i-1,j+1])or VerifCoup(Jeu, Jeu.tabjeu[i,j], Jeu.tabjeu[i-1,j]));
PassTour:=res;
END; { PassTour }


(*--------------------------------------------------------------
 --nom            : VerifMoveBordure1
 --rôle           : Permet de vérifier si un pion peut aller en bordures
 --pré-conditions : Un tableau de jeu et un pion (Type : Game,Pions)
 --résultat       : un booléen
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifMoveBordure1 (Jeu : Game;Pion : pions ): boolean;
VAR
   res : boolean;
BEGIN
   res:=(((Jeu.tabjeu[2,Pion.pos.ordonnee-1].genre<>CaVi)and(Jeu.tabjeu[2,Pion.pos.ordonnee-3].genre<>CaVi)and(Jeu.tabjeu[3,Pion.pos.ordonnee-4].genre=CaVi))or
         ((Jeu.tabjeu[2,Pion.pos.ordonnee+1].genre<>CaVi)and(Jeu.tabjeu[2,Pion.pos.ordonnee+3].genre<>CaVi)and(Jeu.tabjeu[3,Pion.pos.ordonnee+4].genre=CaVi))or
         ((Jeu.tabjeu[9,Pion.pos.ordonnee-1].genre<>CaVi)and(Jeu.tabjeu[9,Pion.pos.ordonnee-3].genre<>CaVi)and(Jeu.tabjeu[8,Pion.pos.ordonnee-4].genre=CaVi))or
         ((Jeu.tabjeu[9,Pion.pos.ordonnee+1].genre<>CaVi)and(Jeu.tabjeu[9,Pion.pos.ordonnee+3].genre<>CaVi)and(Jeu.tabjeu[8,Pion.pos.ordonnee+4].genre=CaVi))or
         ((Jeu.tabjeu[Pion.pos.abscisse+1,2].genre<>Cavi)and(Jeu.tabjeu[Pion.pos.abscisse+3,2].genre<>CaVi)and(Jeu.tabjeu[pion.pos.abscisse+4,3].genre=CaVi))or
         ((Jeu.tabjeu[Pion.pos.abscisse-1,2].genre<>Cavi)and(Jeu.tabjeu[Pion.pos.abscisse-3,2].genre<>CaVi)and(Jeu.tabjeu[pion.pos.abscisse-4,3].genre=CaVi))or
         ((Jeu.tabjeu[Pion.pos.abscisse+1,9].genre<>Cavi)and(Jeu.tabjeu[Pion.pos.abscisse+3,9].genre<>CaVi)and(Jeu.tabjeu[pion.pos.abscisse+4,8].genre=CaVi))or
         ((Jeu.tabjeu[Pion.pos.abscisse-1,9].genre<>Cavi)and(Jeu.tabjeu[Pion.pos.abscisse-3,9].genre<>CaVi)and(Jeu.tabjeu[pion.pos.abscisse-4,8].genre=CaVi)));
   VerifMoveBordure1:=res;
END; { VerifMoveBordure1 }



(*--------------------------------------------------------------
 --nom            : VerifMoveBordure
 --rôle           : Vérifie si un pion peut se rendre en bordure
 --pré-conditions : Un tableau de jeu et un pion (Types : Game, Pions)
 --résultat       : Un booléen (true si c'est possible)
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifMoveBordure (Jeu : Game; Pion :Pions): boolean;
VAR
   res : boolean;
BEGIN
   Case Pion.genre of
     Carre : res:=false;
     Rond  : res:=VerifMoveBordure1(Jeu,Pion);
        Losa  :res:=VerifMoveBordure1(Jeu,Pion);
     Tria  : Case pion.team of
               1 : res :=(((Jeu.tabjeu[2,Pion.pos.ordonnee+1].genre<>CaVi)and(Jeu.tabjeu[2,Pion.pos.ordonnee+3].genre<>CaVi)and(Jeu.tabjeu[3,Pion.pos.ordonnee+4].genre=CaVi))or
                          ((Jeu.tabjeu[9,Pion.pos.ordonnee+1].genre<>CaVi)and(Jeu.tabjeu[9,Pion.pos.ordonnee+3].genre<>CaVi)and(Jeu.tabjeu[8,Pion.pos.ordonnee+4].genre=CaVi)));
               2 : res :=(((Jeu.tabjeu[2,Pion.pos.ordonnee-1].genre<>CaVi)and(Jeu.tabjeu[2,Pion.pos.ordonnee-3].genre<>CaVi)and(Jeu.tabjeu[3,Pion.pos.ordonnee-4].genre=CaVi))or
                          ((Jeu.tabjeu[9,Pion.pos.ordonnee-1].genre<>CaVi)and(Jeu.tabjeu[9,Pion.pos.ordonnee-3].genre<>CaVi)and(Jeu.tabjeu[8,Pion.pos.ordonnee-4].genre=CaVi)));
               3 : res:=(((Jeu.tabjeu[Pion.pos.abscisse+1,2].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse+3,2].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse+4,2].genre=CaVi))or
                          ((Jeu.tabjeu[Pion.pos.abscisse+1,9].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse+3,9].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse+4,3].genre=CaVi)));
               4:res:=(((Jeu.tabjeu[Pion.pos.abscisse-1,2].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse-3,2].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse-4,3].genre=CaVi))or
                          ((Jeu.tabjeu[Pion.pos.abscisse-1,9].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse-3,9].genre<>CaVi)and(Jeu.tabjeu[Pion.pos.abscisse-4,8].genre=CaVi)));
             End; { Case }
   End; { Case }
   VerifMoveBordure:=res;
END; { VerifMoveBordure }



(*--------------------------------------------------------------
 --nom            : VerifSautPion
 --rôle           : permet de vérifier si un pion de n'importe quelle forme peut sauter un pion voisin 
 --pré-conditions : Le jeu actuel, un pion et une case vide (Types : Game, Pions)
 --résultat       : retourne la réponse, de type booleen
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifSautPion (Jeu : Game; pion,caselibre : Pions ):boolean;
Var
   Posi : position;
   res  :  boolean;
BEGIN
   Posi:=SoustraitPos(pion.pos,caselibre.pos);
   if((posi.abscisse>=-2)and(Posi.abscisse<=2)and(Posi.ordonnee>=-2)and(Posi.ordonnee<=2)and
      (caselibre.genre=CaVi)and(Jeu.Tabjeu[(Pion.pos.abscisse+(round(Posi.abscisse/2))),(Pion.pos.ordonnee+(round(Posi.ordonnee/2)))].genre<>CaVi)and
      ((((((Pion.team=1)and(caselibre.pos.ordonnee<>min))or((Pion.team=2)and(caselibre.pos.ordonnee<>max)))and(caselibre.pos.abscisse<max)and(caselibre.pos.abscisse>min))or((((Pion.team=3)and(Caselibre.pos.abscisse<>min))or((Pion.team=4)and(Caselibre.pos.abscisse<>max)))and(caselibre.pos.ordonnee<max)and(caselibre.pos.ordonnee>min)))or VerifMoveBordure(Jeu,Pion)))then
   Begin
      Case pion.genre of
        Carre : res:=(posi.abscisse=0)or(posi.ordonnee=0);
        Rond  : res:=(((posi.abscisse=-2)and(posi.ordonnee=-2))or((posi.abscisse=2)and(posi.ordonnee=-2))or((posi.abscisse=-2)and(posi.ordonnee=2))or((posi.abscisse=2)and(posi.ordonnee=2)))or(posi.abscisse=0)or(posi.ordonnee=0);
        Losa  : res:=(((posi.abscisse=-2)and(posi.ordonnee=-2))or((posi.abscisse=2)and(posi.ordonnee=-2))or((posi.abscisse=-2)and(posi.ordonnee=2))or((posi.abscisse=2)and(posi.ordonnee=2)));
        Tria  : res:=VerifCoupTriangle(Jeu,Pion,Caselibre,2);
      End; { case }
   End
   else
      res:=false;
VerifSautPion:=res;
END; { VerifSautPion }

(*--------------------------------------------------------------
 --nom            : VerifSautCarre
 --rôle           : verifie si un carré peut sauter un pion voisin
 --pré-conditions : un pion carré
 --résultat       : retourne la réponse, de type boolean
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifSautCarre (Jeu : Game; pion :Pions):boolean;
Var
   res : boolean;
BEGIN
   res:=VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee+2])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee-2]);
   VerifSautCarre:=res;
END; { VerifSautCarre }
                                    


(*--------------------------------------------------------------
 --nom            : VerifSautLosange
 --rôle           : verifie si un losange peut sauter un pion voisin
 --pré-conditions : un pion de genre losange
 --résultat       : retourne la réponse, de type boolean
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifSautLosange (Jeu : Game; pion :Pions ):boolean;
Var
   res : boolean;
BEGIN
   res:=VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee-2])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee-2])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee+2])or
   VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee+2]);
   VerifSautLosange:=res;
END; { VerifSautLosange }


(*--------------------------------------------------------------
 --nom            : VerifSautTriangle
 --rôle           : verifie si un triangle  peut sauter un pion 
 --pré-conditions : un pion de genre triangle
 --résultat       : retourne la réponse, de type boolean
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifSautTriangle (Jeu : Game; pion :Pions ):boolean;
Var
res:boolean;
BEGIN
   Case Pion.team of
     1 : res:=  VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee+2])or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee+2]) or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee-2]);
     2 : res:=VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee-2])or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee-2]) or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse,pion.pos.ordonnee+2]);
     3 : res:=VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee-2])or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee+2]) or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee]);
     4 : res:=VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee-2])or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse-2,pion.pos.ordonnee+2]) or
     VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+2,pion.pos.ordonnee]);
   End; { case }
   VerifSautTriangle:=res;
END; { VerifSautTriangle }

(*--------------------------------------------------------------
 --nom            : VerifSautRond
 --rôle           : verifie si un rond peut sauter un pion voisin
 --pré-conditions : un pion de genre rond
 --résultat       : retourne la réponse, de type boolean
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifSautRond (Jeu : Game; pion : Pions ):boolean;
Var
   res : boolean;
BEGIN
   { coordonnées accessibles pour un rond }
   res:=(VerifSautCarre(Jeu, pion)or VerifSautLosange(Jeu, pion));
   VerifSautRond:=res;
END; { VerifSautRond }

(*--------------------------------------------------------------
 --nom            : VerifPossibleJump
 --rôle           : Vérifie si un saut est possible dans le voisinnage d'un pion sélectionné
 --pré-conditions : Le tableau de Jeu et un pion (Types : Game, Pions)
 --résultat       : Un booléen
 --auteur         :  Brugat Paul <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifPossibleJump (Jeu : Game; Pion :Pions ):boolean;
VAR
   res :  boolean;
BEGIN 
   Case Pion.genre of
     Carre : res:=VerifSautCarre(Jeu,Pion);
     Rond  : res:=VerifSautRond(Jeu,Pion);
     Losa  : res:=VerifSautLosange(Jeu,Pion);
     Tria  : res:=VerifSautTriangle(Jeu,Pion);
   else res:=false;
   End; { case }
   VerifPossibleJump:=res;
END; { VerifPossibleJump }








BEGIN
END.


