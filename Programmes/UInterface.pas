
(*
 -------------------------------------------------------------------
 Nom Programme    :  plateau.pas
 Auteur           :  Lescaud Maxime <lescaudmax@eisti.eu>
 Description      :  <description>
 Date de création :  Fri May 20 09:31:40 2016
 Compilation      :  fpc
 Execution        :  shell
 -------------------------------------------------------------------
 *)

Unit  UInterface;
Interface
USES sysutils,UInitialisation,UType,UJeu,crt , gLib2D, dos,SDL, SDL_TTF;

PROCEDURE damier (Jeu : Game );
FUNCTION TeamColor (Numteam,numtheme :integer ):gcolor;
FUNCTION ThemeColor (numtheme,choix :integer ): gcolor;
PROCEDURE gFillLosange (x : integer ; y : integer  ; color : Gcolor );
PROCEDURE gFillTriangle (x : integer; y : integer; equipe  :integer ; color : gColor );
PROCEDURE DispGameTab (Jeu :Game );
PROCEDURE DrawGameTab (Jeu :game );
PROCEDURE Possiblemove (Jeu : Game;Pion:Pions );
PROCEDURE PossibleJump (Jeu : Game; Pion : Pions );
FUNCTION PosToCoord (Posi : Uint16):integer;
FUNCTION MouseControl (Jeu : Game):Position;
PROCEDURE menu ();
PROCEDURE Jouer ();
PROCEDURE Theme ();
Implementation

(*--------------------------------------------------------------
 --nom            : ThemeColor
 --rôle           : Définit les couleurs des différents thèmes
 --pré-conditions : Le numéro du thème ainsi que un entier représentant si la couleur voulue est la principale ou la secondaire ou tertiaire(Type :integer)
 --résultat       : Une couleur (Type : gcolor)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION ThemeColor (numtheme,choix :integer ): gcolor;
Var
   res :  gcolor;
BEGIN
   Case numtheme of
     1 : Case choix of
           1 : res:=WHITE;
           2 : res:=BLACK;
           3 : res:=LITEGRAY;
         END; { case }
     2 : Case choix of
           1 : res:=AZURE;
           2 : res:=BLUE;
           3 : res:=GRAY;
         END; { case }
     3 : Case choix of
           1 : res:=DARKGRAY;
           2 : res:=LITEGRAY;
           3 : res:=GRAY;
         END; { case }
   End; { case }
   ThemeColor:=res;
END; { ThemeColor }


   (*--------------------------------------------------------------
 --nom            : gFillLosange
 --rôle           : créer et place un losange sur le plateau
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE gFillLosange (x : integer ; y : integer  ; color : Gcolor );
var
   i : integer;
BEGIN 
   i:=0; 
   while i<>40 do
   begin
      gDrawLine (x*60-30,y*60-10, x*60-10-i, y*60-30, color);
      i:=i+1;
   end;
   i:=0;
begin
   while i<>40 do
   begin
      gDrawLine ( x*60-30, y*60-50, x*60-10-i, y*60-30, color);
      i:=i+1;
   end;
End;
End; { gFillLosange }
   
(*--------------------------------------------------------------
 --nom            : damier
 --rôle           :  Affiche le damier
 --pré-conditions :  Un jeu (Type : Game)
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE damier (Jeu : Game );
var   
long, larg : integer;
   i,k:integer;
BEGIN 
   gClear(Themecolor(Jeu.theme, 2));
   gfillRect(0,0,600,600,Themecolor(Jeu.theme, 2));
   gfillRect(600,0,200,600,ThemeColor(Jeu.theme,3));
   long:=60;
   larg:=60;
   i:=0;
   while (i<>600) do
   begin
      k:=0;
      While(k<>600)do
      Begin
         gFillRect(i,k,long,larg, ThemeColor(Jeu.theme,1));
         gFillRect(60+i,k+60,long,larg,ThemeColor(Jeu.theme,1));
         k:=k+120;
      end;
      i:=i+120;
   end;
END; { damier }

(*--------------------------------------------------------------
 --nom            : TeamColor
 --rôle           : Permet d'attribuer la couleu d'une équipe
 --pré-conditions : Le numéro de l'équipe (Type : un entier)
 --résultat       : Une couleur (Type : Gcolor )
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION TeamColor (Numteam,numtheme :integer ):gcolor;
VAR
   res : gcolor;
BEGIN
   Case numtheme of   
     1:   Case NumTeam of
            1 : res:=TURQUOISE;
            2 : res:=CHARTREUSE;
            3 : res:=RED;
            4 : res:=ORANGE;
          End; { Case }
     2:   Case NumTeam of
            1 : res:=GREEN;
            2 : res:=YELLOW;
            3 : res:=PINK;
            4 : res:=SALMON;
          End; { Case }
     3:   Case NumTeam of
            1 : res:=BLACK;
            2 : res:=WHITE;
            3 : res:=ALMOND;
            4 : res:=BROWN;
          End; { Case }
   End; { case }
   TeamColor:=res;
END; { TeamColor }

(*--------------------------------------------------------------
 --nom            : gFillTriangle
 --rôle           :  cette procedure permet de créer un triangle à  l'aide la fonction gDrawLine
 --pré-conditions :  prend en entrée les coordonnées du triangle dans le plateau, l'équipe et la couleur
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE gFillTriangle (x : integer; y : integer; equipe  :integer ; color : gColor );
var
   i : integer;
BEGIN
   i:=0;
     while i<>40 do
        begin
           Case equipe of
             1 : gDrawLine (x*60-30,y*60-10, x*60-10-i, y*60-50, color);
             2 : gDrawLine ( x*60-30, y*60-50, x*60-50+i, y*60-10, color);
             3 : gDrawLine ( x*60-10, y*60-30, x*60-50, y*60-10-i, color);
             4 : gDrawLine ( x*60-50, y*60-30, x*60-10, y*60-50+i, color);
             END;{ Case }
           i:=i+1;
        end;
END; { gFillTriangle }

(*--------------------------------------------------------------
 --nom            : DrawGameTab
 --rôle           : Dessine le tableau de jeu actuel
 --pré-conditions : Un tableau de jeu (Type : Game)
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DrawGameTab (Jeu :game );
var
   i,k : integer;
BEGIN
   for i:=1 to 10 do
   Begin
      for k:=1 to 10 do
      Begin
         Case Jeu.tabjeu[i,k].genre of
          Carre : gFillRect(60*i-52,60*k-52,45,45,TeamColor(Jeu.tabjeu[i,k].team,Jeu.theme));
          Rond  : gFillcircle(60*i-30,60*k-30,20,TeamColor(Jeu.tabjeu[i,k].team,Jeu.theme));
          Losa  : gfillLosange(i,k,TeamColor(Jeu.tabjeu[i,k].team,Jeu.theme));
          Tria  : gFillTriangle(i,k,Jeu.tabjeu[i,k].team,TeamColor(Jeu.tabjeu[i,k].team,Jeu.theme));
         end; { case }     
      End;
   End;
END; { DrawGameTab }

(*--------------------------------------------------------------
 --nom            : DispGameTab
 --rôle           :  Affiche le tableau de jeu
 --pré-conditions :  Le tableau de jeu actuel (Type :Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DispGameTab (Jeu :Game );
Var
   Text1,Text2 : gimage ;
   font  : PTTF_Font;
   Tour,Equipe  : integer;
BEGIN
   tour:=Jeu.nbcoups;
   Equipe:=Jeu.nbequipes;
   damier(Jeu);
   DrawGameTab(Jeu);
   font:= TTF_OpenFont('Elevation.ttf', 14);
   text1:= gTextLoad('Tour du joueur  : '+IntToStr(((Tour mod Equipe)+1)), font);
   gBlit(615,300, text1, text1^.w, text1^.h);
   gfillrect(745,300,14,14,teamcolor(((Jeu.nbcoups mod Equipe)+1),Jeu.theme));
   text2:=GtextLoad('Tour : '+IntTostr((Tour div Equipe)),font);
   gBlit(615,550, text2, text2^.w, text2^.h);
   Gflip();
END; { DispGameTab }


(*--------------------------------------------------------------
 --nom            : PossibleJump
 --rôle           : Affiche les sauts possibles d'un pion
 --pré-conditions : Un tableau de jeu et un pion (Types : Game,Pions)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE PossibleJump (Jeu : Game; Pion : Pions );
Var
   i,j : integer;
BEGIN
   DispGameTab(Jeu);
   If(((Pion.pos.abscisse+2<=112)and(Pion.pos.abscisse-2>=-1))and((Pion.pos.ordonnee+2<=12)and(Pion.pos.ordonnee-2>=-1)))then
   Begin
      i:=-2;
      While(i<>4)do
      Begin
         j:=2;
         While (j<>-4)do
         Begin
            If VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+i,pion.pos.ordonnee+j])then
            Begin
               gfillRect(60*(pion.pos.abscisse+i)-60,60*(pion.pos.ordonnee+j)-60,60,60,VIOLET);
               gdrawrect(60*(pion.pos.abscisse+i)-60,60*(pion.pos.ordonnee+j)-60,60,60,Black);
            End;
            If VerifSautPion(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+j,pion.pos.ordonnee+i])then
            Begin      
               gfillRect(60*(pion.pos.abscisse+j)-60,60*(pion.pos.ordonnee+i)-60,60,60,VIOLET);
               gdrawrect(60*(pion.pos.abscisse+j)-60,60*(pion.pos.ordonnee+i)-60,60,60,Black);
            End;
            j:=j-2;
         End;
         i:=i+2;
      End;
   End;
   gflip();
END; { PossibleJump }


(*--------------------------------------------------------------
 --nom            : Possiblemove
 --rôle           : Affiche les mouvements possible d'un pion
 --pré-conditions : Un tableau de jeu et un pion (Type : Game, Pions)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------*)
PROCEDURE Possiblemove (Jeu : Game;Pion:Pions );
Var
   i,j : integer;
BEGIN
   PossibleJump(Jeu,Pion);
   i:=-1;
   While(i<>2)do
   Begin
      j:=1;
      While(j<>-2)do
      Begin
         If VerifCoup(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+i,pion.pos.ordonnee+j])then
         Begin
            gfillRect(60*(pion.pos.abscisse+i)-60,60*(pion.pos.ordonnee+j)-60,60,60,YELLOW);
            gdrawrect(60*(pion.pos.abscisse+i)-60,60*(pion.pos.ordonnee+j)-60,60,60,Black);
         End;
         If VerifCoup(Jeu,Pion,Jeu.tabjeu[pion.pos.abscisse+j,pion.pos.ordonnee+i])then
         Begin      
            gfillRect(60*(pion.pos.abscisse+j)-60,60*(pion.pos.ordonnee+i)-60,60,60,YELLOW);
            gdrawrect(60*(pion.pos.abscisse+j)-60,60*(pion.pos.ordonnee+i)-60,60,60,Black);
         End;
         j:=j-1;
      End;
      i:=i+1;
   End;
   gflip();
END; { Possiblemove }

(*--------------------------------------------------------------
 --nom            : menu
 --rôle           :  affiche le menu pricipal du jeu
 --pré-conditions :  
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE menu ();
var
   text, text1, text2 : gImage;
   font, font1 : PTTF_Font;     
BEGIN
   gclear(black);
   font := TTF_OpenFont('HARRYP.TTF', 150);
   font1 := TTF_OpenFont('font.ttf', 50);
   text:= gTextLoad('Traverse', font);
   text1:= gTextLoad('Jouer', font1);
   text2:= gTextLoad('Theme', font1);
   gBlit(200,50, text, text^.w, text^.h);  
   gFillRect(200,250, 350, 100, LITEGRAY);
   gFillRect(200,450, 350, 100, LITEGRAY);
   gBlit(330,270, text1, text1^.w, text1^.h);
   gBlit(310,470, text2, text2^.w, text2^.h);
   gflip();
END; { menu }

(*--------------------------------------------------------------
 --nom            : Jouer
 --rôle           :  affiche les différentes choix de jeu possible de jeu possible  
 --pré-conditions :  
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE Jouer ();

var
    text, text1, text2, text3 : gImage;
    font, font1 : PTTF_Font;
BEGIN
   gclear(black);
   font := TTF_OpenFont('font.ttf', 100);
   font1 := TTF_OpenFont('font.ttf', 50);
   text := gTextLoad('It''s time to play', font);
   text1 :=gTextLoad('2 joueurs', font1);
   text2  :=gTextLoad('4 joueurs', font1);
   text3  :=gTextLoad('Joueur vs IA', font1);
   font1 := TTF_OpenFont('font.ttf', 20);
   gBlit(150,50, text, text^.w, text^.h);
   gFillRect(100,250, 250, 120, LITEGRAY);
   gFillRect(450,250, 250, 120, LITEGRAY);
   gFillRect(275, 400, 250, 120, LITEGRAY);
   gBlit(130,280, text1, text1^.w, text1^.h);
    gBlit(490,280, text2, text2^.w, text2^.h);
    gBlit(300,430, text3, text3^.w, text3^.h);
   gflip();
END; { Jouer }


(*--------------------------------------------------------------
 --nom            : Theme
 --rôle           :  affiche les différents thémes 
 --pré-conditions :  
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE Theme ();
   var
    text, text1, text2, text3 : gImage;
    font, font1               : PTTF_Font;
   BEGIN                      
      gclear(black);
      font := TTF_OpenFont('font.ttf', 100);
      font1 := TTF_OpenFont('font.ttf', 50);
      text := gTextLoad('Themes', font);
      text1 :=gTextLoad('Classique ', font1);
      text2  :=gTextLoad('Blue', font1);
      text3  :=gTextLoad('Grey ', font1);
      gBlit(250,50, text, text^.w, text^.h);
      gFillRect(100,260, 230, 80, LITEGRAY);
      gFillRect(450,260, 230, 80, LITEGRAY);
      gFillRect(300, 480, 190, 80, LITEGRAY);
      gBlit(130,275, text1, text1^.w, text1^.h);
      gBlit(525,275, text2, text2^.w, text2^.h);
      gBlit(350,490, text3, text3^.w, text3^.h);
      gflip()
END; { Theme }


(*--------------------------------------------------------------
 --nom            : PosToCoord
 --rôle           : Traduit la position d'un pixel en coordonnées sur la tableau de jeu²²
 --pré-conditions : La position d'un pixel sur la fenêtre (Type: Uint16)
 --résultat       : Une coordonnée du tableau de jeu comprise entre 1 et 10 (Type :integer)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION PosToCoord (Posi : Uint16):integer;
VAR
   res : integer;
BEGIN
   res:=(trunc(Posi/60)+1);
   PosToCoord:=res;
END; { PosToCoord }



(*--------------------------------------------------------------
 --nom            : MouseControl
 --rôle           : Retourne la position de la souris sur le tableau de jeu
 --pré-conditions : Un tableau de jeu (Type : Game)
 --résultat       : La position de la souris sur le tableau de jeu (Type : Position)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION MouseControl (Jeu : Game):Position;
VAR
   x          : position;
   clic,clicd : boolean;
BEGIN
   clic:=false;
   clicd:=false;
   while  (clic=false)and(clicd=false)do
   BEGIN
      while (sdl_update = 1) DO
      BEGIN
         if (sdl_do_quit) then exit;
         if sdl_mouse_left_down then clic:= true;
         if sdl_mouse_right_down then clicd:=true;
      END;
      If (clic=true) then
         x:=AssignPos(PosToCoord(sdl_get_mouse_x()),PosToCoord(sdl_get_mouse_y()));
      If(Clicd)then
         x:=AssignPos(0,0);
      MouseControl:=x;
   End;
End; { MouseControl }


BEGIN
END.
