Unit UType;
interface
Const
   min        = 1;
   max        = 10;
   Carre    = 'Carré';
   Rond     = 'Rond';
   Losa  = 'Losange';
   Tria = 'Triangle';
   CaVi = 'CaseVide';
   
Type
{ Structure de données permettant l'enregistrement de la position de chaque pion }
Position  = record
               abscisse : integer;
               ordonnee : integer;
            end;        
  { Structure de données de gestion du type de pions et de leurs déplacements }
   Pions = record
              team  : integer;
              genre : string;
              Pos   : Position;
           end; { Pions }
{Tableau représentant le tableau de jeu }
GameTab = array [min..max]of array [min..max] of Pions;
{Structure de jeu contenant tous les tableaux de jeu du debut à la fin de la partie }
 PtrJeu=record
           Valeur  : GameTab;
           suivant : ^PtrJeu;
        end;
   PtrInit=^PtrJeu;
{ Structure de données prenant compte du tableau de jeu et le nombre de coups }
Game=record
        TabJeu       : GameTab;
        nbcoups      : integer;
        ListeDeCoups : PtrInit;
        nbequipes    : integer;
        theme        : integer;
     end;            

implementation

BEGIN
END.
