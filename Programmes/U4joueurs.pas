Unit U4joueurs;
interface
Uses UType,UInitialisation,UJeu,UInterface,U2joueurs;
FUNCTION VerifEquipe4joueurs (Equipe :integer ):integer;
FUNCTION PlacePions4joueurs (Tabjeu : Gametab;Equipe :integer ):Gametab;
FUNCTION Victoire4Joueurs (Jeu :Game ):Boolean;
implementation
(*--------------------------------------------------------------
 --nom            : VerifEquipe4joueurs
 --rôle           : Verifie le numéro d'une équipe (cas 4 joueurs )
 --pré-conditions : Le numéro d'une équipe (un entier)
 --résultat       : Le numéro d'une équipe modifié si nécessaire (un entier)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifEquipe4joueurs (Equipe :integer ):integer;
VAR
   res : integer;
BEGIN
   If(Equipe<>1)then
   Begin
      res:=4;
   end
   else
      Begin
      Res:=3;
      End;
   VerifEquipe4joueurs:=res;
END;

(*--------------------------------------------------------------
 --nom            : PlacePion4joueurs
 --rôle           : Permet de placer les pions pour une partie à 4 joueurs
 --pré-conditions : Un tableau de jeu initialisé et le numéro d'une équipe(Types :Gametab, integer)
 --résultat       : Un tableau de jeu avec des pions placés (Type : GameTab)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION PlacePions4joueurs (Tabjeu : Gametab;Equipe :integer ):Gametab;
VAR
   Res : GameTab;
BEGIN
   Res:=Tabjeu;
   Res[Equipe,2]:=Square(VerifEquipe4joueurs(Equipe));
   Res[Equipe,2].pos:=AssignPos(Equipe,2);
   Res[Equipe,3]:=Triangle(VerifEquipe4joueurs(Equipe));
   Res[Equipe,3].pos:=AssignPos(Equipe,3);
   Res[Equipe,4]:=Losange(VerifEquipe4joueurs(Equipe));
   Res[Equipe,4].pos:=AssignPos(Equipe,4);
   Res[Equipe,5]:=Circle(VerifEquipe4joueurs(Equipe));
   Res[Equipe,5].pos:=AssignPos(Equipe,5);
   Res[Equipe,6]:=Circle(VerifEquipe4joueurs(Equipe));
   Res[Equipe,6].pos:=AssignPos(Equipe,6);
   Res[Equipe,7]:=Losange(VerifEquipe4joueurs(Equipe));
   Res[Equipe,7].pos:=AssignPos(Equipe,7);
   Res[Equipe,8]:=Triangle(VerifEquipe4joueurs(Equipe));
   Res[Equipe,8].pos:=AssignPos(Equipe,8);
   Res[Equipe,9]:=Square(VerifEquipe4joueurs(Equipe));
   Res[Equipe,9].pos:=AssignPos(Equipe,9);
   PlacePions4joueurs:=res;
END; { PlacePions4joueurs }

(*--------------------------------------------------------------
 --nom            : Victoire4Joueurs
 --rôle           :  Permet de définir la victoire d'un joueur 
 --pré-conditions : Un tableau de jeu (Type : Game)
 --résultat       : Un booleén (True = victoire)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Victoire4Joueurs (Jeu :Game ):Boolean;
VAR
   i,WinTeamI,WinTeamII,WinTeamIII,WinTeamIV,LoseTeamI,LoseTeamII,LoseTeamIII,LoseTeamIV : integer;
BEGIN
   WinTeamI:=0;
   WinTeamII:=0;
   WinTeamIII:=0;
   WinTeamIV:=0;
   LoseTeamI:=0;
   LoseTeamII:=0;
   LoseTeamIII:=0;
   LoseTeamIV:=0;
   For i:= min to max do
   Begin
      Case Jeu.Tabjeu[i,min].team of
        1 : LoseTeamI:=LoseTeamI+1;
        2 : WinTeamII:=WinTeamII+1;
      End;{ case }
      Case Jeu.Tabjeu[i,max].team of
        1 : WinTeamI:=WinTeamI+1;
        2 : LoseTeamII:=LoseTeamII+1;
      End; { case }
      Case Jeu.Tabjeu[min,i].team of
        3 : LoseTeamIII:=LoseTeamIII+1;
        4 : WinTeamIV:=WinTeamIV+1;
      End;{ case }
      Case Jeu.Tabjeu[max,i].team of
        3 : WinTeamIII:=WinTeamIII+1;
        4 : LoseTeamIV:=LoseTeamIV+1;
      End; { case }
   End;
   Victoire4Joueurs:=(((Jeu.nbcoups>120)and((LoseTeamI>0)or(LoseTeamII>0)or(LoseTeamIII>0)or(LoseTeamIV>0)))or(WinteamI=8)or(WinteamII=8)or(WinteamIII=8)or(WinteamIV=8));
END; { Victoire4Joueurs }


BEGIN
END.
