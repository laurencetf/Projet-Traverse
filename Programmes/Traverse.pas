
(*
 -------------------------------------------------------------------
 Nom Programme    :  traverse.pas
 Auteur           :  Laurencet Fabien <laurencetf@eisti.eu>
 Description      : Programme principal du jeu Traverse
 Date de création :  Tue Jun  7 21:53:21 2016
 Compilation      :  fpc
 Execution        :  shell
 -------------------------------------------------------------------
 *)

PROGRAM  traverse;
USES crt,UType,UInitialisation,UJeu,UInterface,U2joueurs,U4joueurs, glib2d;

(*--------------------------------------------------------------
 --nom            : Play
 --rôle           : Permet de lancer le jeu 
 --pré-conditions : Un jeu initialisé de (Type : Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE Play (Jeu :Game );
var
   ptr   : PtrInit;
   x,y,z : position;
BEGIN  
   WHILE(not(Victoire2Joueurs(Jeu))and not(Victoire4joueurs(Jeu))and not(Verifnul(jeu)))DO
   BEGIN
      DispGameTab(Jeu);
      if(not PassTour(Jeu))then
      Begin
         Jeu.nbcoups:=Jeu.nbcoups+1;
      end
      else
         Begin
            x:=MouseControl(Jeu);
            If((Jeu.tabjeu[x.abscisse,x.ordonnee].genre<>CaVi)and(Jeu.tabjeu[x.abscisse,x.ordonnee].team=((Jeu.nbcoups mod Jeu.nbEquipes)+1)))then
            Begin
               Possiblemove(Jeu,Jeu.tabjeu[x.abscisse,x.ordonnee]);
               y:=MouseControl(Jeu);
               If((Jeu.tabjeu[y.abscisse,y.ordonnee].genre=CaVi)and(VerifCoup(Jeu,Jeu.tabjeu[x.abscisse,x.ordonnee],Jeu.tabjeu[y.abscisse,y.ordonnee]))or(VerifSautPion(Jeu,Jeu.tabjeu[x.abscisse,x.ordonnee],Jeu.tabjeu[y.abscisse,y.ordonnee])))then
               Begin
                  Jeu:=EchangePions(Jeu,Jeu.tabjeu[x.abscisse,x.ordonnee],Jeu.tabjeu[y.abscisse,y.ordonnee]);
                  z:=SoustraitPos(Jeu.tabjeu[x.abscisse,x.ordonnee].pos,Jeu.tabjeu[y.abscisse,y.ordonnee].pos);
                  if((z.abscisse>1)or(z.abscisse<-1)or(z.ordonnee>1)or(z.ordonnee<-1))then                  
                     Jeu:=Exemove(Jeu,Jeu.Tabjeu[y.abscisse,y.ordonnee]);
                  New(Ptr);
                  Ptr^.valeur:=Jeu.tabjeu;
                  New(Jeu.listedecoups);
                  Jeu.ListeDeCoups:=InsertFin(Jeu.listedecoups,Ptr);
               End;
            End;
         End;
   End;
END; { Play }

(*--------------------------------------------------------------
 --nom            : SaveData
 --rôle           : Écrit dans un fichier les données d'un jeu
 --pré-conditions : un fichier et un jeu actuel(Types :Text, Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE SaveData (Var fic : text; Jeu : Game );
Var
   i,j : integer;
BEGIN
   Writeln(fic,'#Partie Traverse sauvegardée');
   for i:=min to max do
      for j:=min to max do
         Begin
            Write(fic,Jeu.tabjeu[i,j].genre,' ');
            Writeln(fic,Jeu.tabjeu[i,j].team,' ');
         End;
   Writeln(fic,Jeu.nbcoups);
   Writeln(fic,'#Sauvegarde des coups précédents');
   While(Jeu.listedecoups^.suivant<>nil)do
      Begin
         for i:=min to max do
            for j:=min to max do
               Writeln(fic,Jeu.listedecoups^.valeur[i,j].genre,' ',Jeu.listedecoups^.valeur[i,j].team);
         Readln(fic);
         Jeu.listedecoups:=Jeu.listedecoups^.suivant;
      End;
END; { SaveData }


(*--------------------------------------------------------------
 --nom            : SaveGame
 --rôle           : Permet la sauvegarde d'une partie 
 --pré-conditions : Un Jeu Actuel (Type : Game)
 --résultat       : Un entier représentant le résultat de la sauvegarde
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION SaveGame (Jeu :Game ):integer;
VAR
   fic : text;
BEGIN
   SaveGame:= 1;
   assign(fic,'Save.txt');
   rewrite(fic);
   if (IOresult<>0) then
   begin
      SaveGame:=0;
   end
else
begin
   SaveData(fic,Jeu);
   close(fic);
end;
END; { SaveGame }

(*--------------------------------------------------------------
 --nom            : ReadData
 --rôle           : Lis les données d'un fichier text
 --pré-conditions : Un fichier text contenant les données sauvegardées (Type : text)
 --résultat       : Un tableau de jeu (Type : Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION ReadData (Var fic : text ):Gametab;
VAR
   res : Gametab;
   i,j : integer;
BEGIN
   i:=min;
   While(i<>max+1)do
      Begin
         j:=min;
         While(j<>max+1)do
            Begin
               Read(fic,Res[i,j].genre);
               Read(fic,Res[i,j].team);
               Res[i,j].pos:=AssignPos(i,j);
               j:=j+1;
            End;
         i:=i+1;
      End;
   ReadData:=res;
END; { ReadData }




(*--------------------------------------------------------------
 --nom            : LoadGame
 --rôle           : Récupère une partie sauvegardée dans un fichier
 --pré-conditions : Un fichier (Type : Text)
 --résultat       : Un tableau de jeu (Type : Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION LoadGame (Var fic :text ):Game;
VAR
   res : Game;
   Ptr : PtrInit;
BEGIN
   Assign(fic,'Save.txt');
   Reset(fic);
   Readln(fic);
   Res.tabjeu:=ReadData(fic);
   Read(fic,Res.nbcoups);
   readln(fic);
   While(not(eof))do
   Begin
      New(Ptr);
      Ptr^.valeur:=ReadData(fic);
      Ptr^.suivant:=nil;
      Res.listedecoups:=InsertFin(Res.listedecoups,Ptr);
      Readln(fic);
   End;
   LoadGame:=Res
END; { LoadGame }



(*--------------------------------------------------------------
 --nom            : Click
 --rôle           : Retourne la position de la souris 
 --pré-conditions : aucunes
 --résultat       : La position de la souris après un clic
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Click ():Position;
VAR
   x          : position;
   clic : boolean;
BEGIN
   clic:=false;
   while  (clic=false)do
   BEGIN
      while (sdl_update = 1) DO
      BEGIN
         if (sdl_do_quit) then exit;
         if sdl_mouse_left_down then clic:= true;
         if sdl_mouse_left_up then clic:= false;
      END;
      If (clic=true) then
         x:=AssignPos(sdl_get_mouse_x(),sdl_get_mouse_y());
      Click:=x;
   End;
END; { Click }


(*--------------------------------------------------------------
 --nom            : InterfaceJouer()
 --rôle           : Permet la transition entre le menu et le jeu
 --pré-conditions :  Un jeu initialisé (Type :Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE InterfaceJouer(Jeu : game );
Var
   x : position;
BEGIN
   Jouer();
   x:=Click();
   x:=Assignpos(x.abscisse,x.ordonnee);
   If(x.abscisse>=100)and(x.abscisse<=350)and(x.ordonnee>=250)and(x.ordonnee<=370)then
      Play(Jeu);
      If(x.abscisse>=450)and(x.abscisse<=700)and(x.ordonnee>=250)and(x.ordonnee<=370)then
      Begin
         Jeu.tabjeu:=PlacePions4joueurs( Jeu.tabjeu,min);
         Jeu.tabjeu:=PlacePions4joueurs(Jeu.tabjeu,max);
         Jeu.nbequipes:=4;
         Play(Jeu);
      End;
  (* If(x.abscisse>=275)and(x.abscisse<=525)and(x.ordonnee>=400)and(x.ordonnee<=520)then
   IA... *)
END; { InterfaceJouer }

(*--------------------------------------------------------------
 --nom            : InterfaceTheme
 --rôle           : Permet de changer le thème de jeu
 --pré-conditions : Un jeu initialisé (Type : Game)
 --résultats      : le numéro du thème (Type : integer;
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
function InterfaceTheme (Jeu : Game ): Game;
Var
   x :  position;
BEGIN
   InterfaceTheme:=Jeu;
   Theme();
   x:=Click();
   x:=Assignpos(x.abscisse,x.ordonnee);
   If(x.abscisse>=100)and(x.abscisse<=330)and(x.ordonnee>=260)and(x.ordonnee<=340)then
      InterfaceTheme.theme:=1;
   If(x.abscisse>=450)and(x.abscisse<=680)and(x.ordonnee>=260)and(x.ordonnee<=340)then
      InterfaceTheme.theme:=2;
   If(x.abscisse>=300)and(x.abscisse<=490)and(x.ordonnee>=480)and(x.ordonnee<=560)then
      InterfaceTheme.theme:=3;
END; { InterfaceTheme }



      
(*--------------------------------------------------------------
 --nom            : InterfaceMenu
 --rôle           : Permet l'utilisation du menu et la navigation entre les différentes pages de celui-ci
 --pré-conditions :  Un Jeu initialisé (Type : Game)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE InterfaceMenu (Jeu :Game );
Var
   x :  position;
BEGIN
   While (not(sdl_do_quit))do
   Begin
      Menu();
      x:=Click();
      x:=Assignpos(x.abscisse,x.ordonnee);
      If(x.abscisse>=200)and(x.abscisse<=550)and(x.ordonnee>=250)and(x.ordonnee<=350)then
         InterfaceJouer(Jeu);
      If(x.abscisse>=200)and(x.abscisse<=550)and(x.ordonnee>=450)and(x.ordonnee<=550)then
         Jeu:=InterfaceTheme(Jeu);
   End;
END; { InterfaceMenu }







Var
   Jeu      : Game;
   nbEquipe : integer;
BEGIN
   Nbequipe:=2;
   Jeu:=InitGameTab(Nbequipe);
   InterfaceMenu(Jeu);
END.
