Unit Uinitialisation;

Interface
Uses UType;
  
FUNCTION InitGameTab (nbEquipe : integer ): Game;
FUNCTION Circle (Equipe      : integer ):Pions;
FUNCTION Square (Equipe : integer ):Pions;
FUNCTION Losange (Equipe : integer ):Pions;
FUNCTION Triangle (Equipe : integer ):Pions;
FUNCTION CaseVide ():Pions;
FUNCTION VerifEquipe (Equipe :integer ):integer;
FUNCTION PlacePions (Tab : GameTab; Equipe :integer ):GameTab;
FUNCTION AssignPos (x,y : integer ): Position;
FUNCTION InsertFin (l,nv :PtrInit ):PtrInit;
FUNCTION Suppdernier (l :PtrInit ):PtrInit;
implementation

(*--------------------------------------------------------------
 --nom            : Circle
 --rôle           : Initialise un pion rond
 --pré-conditions :  Le numéro de l'équipe (un entier )
 --résultat       : Un pion de forme rond ( de type Pions )
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Circle (Equipe : integer ):Pions;
var
   res : Pions;
BEGIN
   Res.team:=(Equipe);
   Res.genre:=Rond;
   Res.pos.abscisse:=0;
   Res.pos.ordonnee:=0;
   Circle:=res;
END; { Circle }



(*--------------------------------------------------------------
 --nom            : Square
 --rôle           : Initialise un pion carré
 --pré-conditions :  Le numéro de l'équipe (un entier )
 --résultat       : Un pion de forme carré ( de type Pions )
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Square (Equipe : integer ):Pions;
var
   res : Pions;
BEGIN
   Res.team:=(Equipe);
   Res.genre:=Carre;
   Res.pos.abscisse:=0;
   Res.pos.ordonnee:=0;
   Square:=res;
END; { Square }



(*--------------------------------------------------------------
 --nom            : Losange
 --rôle           : Initialise un pion Losange
 --pré-conditions :  Le numéro de l'équipe (un entier )
 --résultat       : Un pion de forme Losange ( de type Pions )
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Losange (Equipe : integer ):Pions;
var
   res : Pions;
BEGIN
   Res.team:=(Equipe);
   Res.genre:=Losa;
   Res.pos.abscisse:=0;
   Res.pos.ordonnee:=0;
   Losange:=res;
END; { Losange }



(*--------------------------------------------------------------
 --nom            : Triangle
 --rôle           : Initialise un pion de forme Triangle
 --pré-conditions :  Le numéro de l'équipe (un entier )
 --résultat       : Un pion de forme triangle ( de type Pions )
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Triangle (Equipe : integer ):Pions;
var
   res : Pions;
BEGIN
   Res.team:=(Equipe);
   Res.genre:=Tria;
   Res.pos.abscisse:=0;
   Res.pos.ordonnee:=0;
   Triangle:=res;
END; { Triangle }



(*--------------------------------------------------------------
 --nom            : CaseVide
 --rôle           : Initialise une case vide
 --pré-conditions :  aucunes
 --résultat       : Une Case Vide ( de Type Pions)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION CaseVide ():Pions;
Var
   res : Pions;
BEGIN
   Res.genre:=CaVi;
   Res.pos.abscisse:=0;
   Res.pos.ordonnee:=0;
   Res.team:=0;
   CaseVide:=res;
END; { CaseVide }



(*--------------------------------------------------------------
 --nom            : VerifEquipe
 --rôle           : Verifié le numéro d'une équipe
 --pré-conditions : Le numéro d'une équipe (un entier)
 --résultat       : Le numéro d'une équipe modifié si nécessaire (un entier)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifEquipe (Equipe :integer ):integer;
VAR
   res : integer;
BEGIN
   If(Equipe<>1)then
   Begin
      res:=2;
   end
   else
      Begin
      Res:=1;
      End;
   VerifEquipe:=res;
END; { VerifEquipe }



(*--------------------------------------------------------------
 --nom            : AssignPos
 --rôle           : Assigne la position d'un pions en fonction de ses coordonnées
 --pré-conditions : Les coordonnées du pions : abscisse et ordonnee (deux entiers)
 --résultat       : La position d'un pion : abscisse et ordonnée (structure de données de deux entiers (de type Position))
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION AssignPos (x,y : integer ): Position;
Var
   Res : position;
BEGIN
   Res.abscisse:=x;
   Res.ordonnee:=y;
   AssignPos:=Res;
END; { AssignPos }

     

(*--------------------------------------------------------------
 --nom            : PlacePions
 --rôle           : Place les pions sur le tableau de jeu
 --pré-conditions : Le côté dans lequel placer les pions selon le numéro de l'équipe
 --résultat       : Un tableau de jeu avec les pions placés
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION PlacePions (Tab : GameTab; Equipe :integer ):GameTab;
Var
   res : GameTab;
BEGIN
   Res:=Tab;
   Res[2,Equipe]:=Square(VerifEquipe(Equipe));
   Res[2,Equipe].pos:=AssignPos(2,Equipe);
   Res[3,Equipe]:=Triangle(VerifEquipe(Equipe));
   Res[3,Equipe].pos:=AssignPos(3,Equipe);
   Res[4,Equipe]:=Losange(VerifEquipe(Equipe));
   Res[4,Equipe].pos:=AssignPos(4,Equipe);
   Res[5,Equipe]:=Circle(VerifEquipe (Equipe));
   Res[5,Equipe].pos:=AssignPos(5,Equipe);
   Res[6,Equipe]:=Circle(VerifEquipe (Equipe));
   Res[6,Equipe].pos:=AssignPos(6,Equipe);
   Res[7,Equipe]:=Losange(VerifEquipe (Equipe));
   Res[7,Equipe].pos:=AssignPos(7,Equipe);
   Res[8,Equipe]:=Triangle(VerifEquipe (Equipe));
   Res[8,Equipe].pos:=AssignPos(8,Equipe);
   Res[9,Equipe]:=Square(VerifEquipe (Equipe));
   Res[9,Equipe].pos:=AssignPos(9,Equipe);
   PlacePions:=res;
END; { PlacePions }




(*--------------------------------------------------------------
 --nom            : InsertFin
 --rôle           : Insert un maillon à la fin d'une chaîne de pointeurs
 --pré-conditions : une liste initiale de pointeurs, un nouveau maillon de type PtrInit
 --résultat       : un pointeur initial d'une liste chaînée (Type : PtrInit)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION InsertFin (l,nv :PtrInit ):PtrInit;
VAR
   lpremier :PtrInit;
BEGIN
   lpremier:=l;
   while (l^.suivant<>nil)do
   Begin
      l:=l^.suivant;
   End;
   nv^.suivant:=l^.suivant;
   l^.suivant:=nv;
   InsertFin:=lpremier;
END; { InsertFin }

(*--------------------------------------------------------------
 --nom            : Suppdernier
 --rôle           :  décrire le but de la fonction
 --pré-conditions :  aucunes
 --résultat       :  ce que retourne la fonction, et le type
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Suppdernier (l :PtrInit ):PtrInit;
VAR
   lpremier : PtrInit;
BEGIN
   New(lpremier);
   lpremier:=l;
   while(l^.suivant<>nil)do
   begin
      l:=l^.suivant;
   end;
   SuppDernier:=lpremier;
   dispose(l);
END; { Suppdernier }





(*--------------------------------------------------------------
 --nom            : InitGameTab
 --rôle           : Initialise le tableau de jeu
 --pré-conditions : le nombre d'équipes en jeu (Type : Integer)
 --résultat       :  Un tableau de jeu avec les pions placés à leurs places initiales
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION InitGameTab (nbEquipe : integer ): Game;
VAR
   i,j : integer;
BEGIN
   For i :=min to max do
   Begin
      For j:=min to max do
      Begin
         InitGameTab.TabJeu[i,j]:=CaseVide();
         InitGameTab.Tabjeu[i,j].pos.abscisse:=i;
         InitGameTab.Tabjeu[i,j].pos.ordonnee:=j;
      End;
   End;
   InitGameTab.nbequipes:=nbEquipe;
   InitGameTab.TabJeu:=PlacePions(InitGameTab.TabJeu,Min);
   InitGameTab.TabJeu:=PlacePions(InitGameTab.TabJeu,Max);
   InitGameTab.nbcoups:=0;
   InitGameTab.theme:=1;
   New(InitGameTab.ListeDeCoups);
   InitGameTab.ListeDeCoups^.valeur:=InitGameTab.Tabjeu;
   InitGameTab.ListeDeCoups^.suivant:=nil;
END; { InitGameTab }



BEGIN
END.
